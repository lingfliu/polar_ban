function [E_z E_phi E_rho H_z H_phi H_rho]= calc_scatter_field(para, field_para, dimen)

%solve the parameters at k_z for all cylinders 
%return: a_m, b_m, c_m, d_m of each cylinder

%set parameters
f = para.f;
w = para.w;
I = para.I;
ep0 = para.ep0;
mu0 = para.mu0;
ep = para.ep;
mu = para.mu;

kz = para.kz;
k0 = para.k0;
k = para.k;
[k0_rho k_rho] = calc_k(k0, k, kz);

dA = para.dA;
dBdA = para.dBdA;


order = -para.order:para.order;

pos_c = para.pos_c;
pos_s = para.pos_s;
r_c = para.r_c;
num_c = length(para.r_c);

%para: (kz, p, m, 4)

x = dimen.x;
y = dimen.y;
z = dimen.z;

for p = 1:num_c
	[phi_s(p) rho_s(p)] = cart2pol(pos_s(1)-pos_c(p,1), pos_s(2)-pos_c(p,2));
end

E_z_k = zeros(length(x), num_c, length(kz));
E_phi_k = zeros(length(x), num_c, length(kz));
E_rho_k = zeros(length(x), num_c, length(kz));
H_z_k = zeros(length(x), num_c, length(kz));
H_phi_k = zeros(length(x), num_c, length(kz));
H_rho_k = zeros(length(x), num_c, length(kz));

E_z = zeros(length(x), num_c);
E_phi = zeros(length(x), num_c);
E_rho = zeros(length(x), num_c);
H_z = zeros(length(x), num_c);
H_phi = zeros(length(x), num_c);
H_rho = zeros(length(x), num_c);

for km = 1:length(kz)

    fprintf(sprintf('scatter field calculation percent = %.2f\n',km/length(kz)*100));
    for p = 1:num_c
        
        for idx = 1:length(x)
            zm = z(idx);
                        
            phi = [0 0 0];
            rho = [0 0 0];
            for dd = 1:num_c
                [phi(dd) rho(dd)] = cart2pol(x(idx)-pos_c(dd,1),y(idx)-pos_c(dd,2));
            end
            
                
            isinside = -1;
            for q = [1:p-1 p+1:num_c]
                if (rho(q) < r_c(q))
                    isinside = 1;
                    break;
                end
            end
            
            if (isinside > 0)
                continue;
            end
                
            mkz = order*kz(km)/rho(p);
            jwmu0 = 1i*w*mu0;
            jwmu = 1i*w*mu;
            jwep0 = 1i*w*ep0;
            jwep = 1i*w*ep;
            wep0m = w*mu0*order/rho(p);
            wepm = w*mu*order/rho(p);
            
            E_z_k(idx,p,km) = 0;
            E_phi_k(idx,p,km) = 0;
            E_rho_k(idx,p,km) = 0;
            H_z_k(idx,p,km) = 0;
            H_phi_k(idx,p,km) = 0;
            H_z_k(idx,p,km) = 0;
            
            phase = exp(1i*order*(phi(p)-phi_s(p)));
                                    
            if (rho(p) >= r_c(p))
                %Outside
                H = besselh(order,2,rho(p)*k0_rho(km)).*phase;
                Hd = besselhd(order,2,rho(p)*k0_rho(km)).*phase;
                Ye = squeeze(field_para(km,p,:,2));
                Yh = squeeze(field_para(km,p,:,4));
                %para: (kz, p, m, 4)
                E_z_k(idx,p,km) = H*Ye;
                E_phi_k(idx,p,km) = (mkz.*H*Ye + jwmu0*k0_rho(km)*Hd*Yh)/k0_rho(km)^2;
                E_rho_k(idx,p,km) = (-1i*kz(km)*k0_rho(km)*Hd*Ye+w*mu0/rho(p)*order.*H*Yh)/k0_rho(km)^2;
                
                H_z_k(idx,p,km) = H*Yh;
                H_phi_k(idx,p,km) = (mkz.*H*Yh - jwep0*k0_rho(km)*Hd*Ye)/k0_rho(km)^2;
                H_rho_k(idx,p,km) = (-1i*kz(km)*k0_rho(km)*Hd*Yh - k0_rho(km)*wep0m.*H*Ye)/k0_rho(km)^2;
            else
                %Inside
                J = besselj(order,rho(p)*k_rho(km)).*phase;
                Jd = besseljd(order,rho(p)*k_rho(km)).*phase;
                Xe = squeeze(field_para(km,p,:,1));
                Xh = squeeze(field_para(km,p,:,3));
                %para: (kz, p, m, 4)
                E_z_k(idx,p,km) = (J*Xe);
                E_phi_k(idx,p,km) = (mkz.*J*Xe + jwmu*k_rho(km)*Jd*Xh)/k_rho(km)^2;
                E_rho_k(idx,p,km) = (-1i*kz(km)*k_rho(km)*Jd*Xe+w*mu/rho(p)*order.*J*Xh)/k_rho(km)^2;
                
                H_z_k(idx,p,km) = (J*Xh);
                H_phi_k(idx,p,km) = (mkz.*J*Xh - jwep*k_rho(km)*Jd*Xe)/k_rho(km)^2;
                H_rho_k(idx,p,km) = (-1i*kz(km)*k_rho(km)*Jd*Xh - k_rho(km)*wepm.*J*Xe)/k_rho(km)^2;
            end
            
            phaseKzZ = exp(-1i*kz(km)*zm);
            E_z_k(idx,p,km) = E_z_k(idx,p,km)*phaseKzZ;
            E_phi_k(idx,p,km) = E_phi_k(idx,p,km)*phaseKzZ;
            E_rho_k(idx,p,km) = E_rho_k(idx,p,km)*phaseKzZ;
            
            H_z_k(idx,p,km) = H_z_k(idx,p,km)*phaseKzZ;
            H_phi_k(idx,p,km) = H_phi_k(idx,p,km)*phaseKzZ;
            H_rho_k(idx,p,km) = H_rho_k(idx,p,km)*phaseKzZ;
        end                
    end %idx
      a = 1;  
end

%Integration
phase_int = (1+1i*dBdA)*dA/(2*pi);
for idx = 1:length(x)
    for p = 1:num_c  
        E_z(idx,p) = (phase_int*squeeze(E_z_k(idx,p,:)));
        E_phi(idx,p) = (phase_int*squeeze(E_phi_k(idx,p,:)));
        E_rho(idx,p) = (phase_int*squeeze(E_rho_k(idx,p,:)));
        
        H_z(idx,p) = (phase_int*squeeze(H_z_k(idx,p,:)));
        H_phi(idx,p) = (phase_int*squeeze(H_phi_k(idx,p,:)));
        H_rho(idx,p) = (phase_int*squeeze(H_rho_k(idx,p,:)));
    end
end

a = 10;

end