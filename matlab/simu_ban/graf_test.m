clear;clc;close all;
warning off all;

order = -100:100;
order_ex = -200:200;

para = linspace(1,10,length(order));

r = 0.3;
d = 0.6;
k = 51;
pos_q = [0, 1];
pos_p = [1.5, 1.5];
pos = [1.2,1.2]
pos_s = [-1 0];

[phi rho] = cart2pol(pos(1)-pos_q(1), pos(2)-pos_q(2)); % in q
[phi_p rho_p] = cart2pol(pos(1)-pos_p(1), pos(2)-pos_p(2)); % in p

[phi_pq rho_pq] = cart2pol(pos_p(1)-pos_q(1), pos_p(2)-pos_q(2)); % p in q

for m = 1:length(order)
    t(m) = sum(besselh(order(m)+order_ex,2,k*rho_pq).*besselj(order_ex,k*rho_p).*exp(order_ex*1i*(pi-phi_p+phi_pq)));
end
u = besselh(order,2,k*rho).*exp(order*1i*(phi-phi_pq));
plot(abs(u-t))
