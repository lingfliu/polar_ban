This is the multi-cylinder scattering simulation matlab code for on-body channel propagation and field distribution of WBAN.

Project reinitiated on 2016.01. Under the sponsoring of NSFC. 

Current version is v.5.4.

Changes:

1. Added polaration distribution analysis for static body.

Todo:
1. Support elliptical cylinder scattering simulation.
2. Support finite-length line source integration computing