function field_para = solve_field(para, kz)
warning off all;
%solve the parameters at k_z for all cylinders
%return: a_m, b_m, c_m, d_m of each cylinder

%set parameters
% f = para.f;
w = para.w;
% I = para.I;
ep0 = para.ep0;
mu0 = para.mu0;
ep = para.ep;
mu = para.mu;

[k0_rho k_rho] = calc_k(para.k0, para.k, kz);

order = -para.order:para.order;
order_ex = order(1)*2:order(end)*2; %for multi-cyllinder scattering

ite = para.iter;

num_c = length(para.r_c);

pos_s = para.pos_s; %source position [x y z]
pos_c = para.pos_c; % cylinder position [x1 y1; x2 y2; ...]
r_c = para.r_c; % radius cylinder [r1 r2 r3]

%phi_c, rho_c (p,q) coordinate of cylinder p in q
for p = 1:num_c
    [phi_s(p) rho_s(p)] = cart2pol(pos_s(1) - pos_c(p,1), pos_s(2) - pos_c(p,2));
    for q = 1:num_c
        [phi_c(p,q) rho_c(p,q)] = cart2pol(pos_c(p,1)-pos_c(q,1),pos_c(p,2)-pos_c(q,2)); %Polar coordinate O_p in O_q
    end
end

if (num_c <= 1)
    %Single cylinder scattering
    
    field_para = zeros(num_c, length(order), 4);
    Beta = zeros(num_c,4,length(order));
    
    %Beta is the incident matrix
    Beta(1,:,:) = calc_incident_matrix(para, pos_s, pos_c, r_c, kz);
    H_r = besselh(order, 2, k0_rho*r_c);
    H_r_d = besselhd(order, 2, k0_rho*r_c);
    J_r = besselj(order, k_rho*r_c);
    J_r_d = besseljd(order, k_rho*r_c);
    
    mkz = order*kz/(k_rho^2*r_c);
    mkz0 = order*kz/(k0_rho^2*r_c);       
    jwmu = 1i*w*mu/k_rho;
    jwmu0 = 1i*w*mu0/k0_rho;
    jwep = 1i*w*ep/k_rho;
    jwep0 = 1i*w*ep0/k0_rho;
    
    for m = 1:length(order)
        %Calculate the incident boundary vector given I_x I_y I_z
        Lambda = [         J_r(m)           -H_r(m)            0                  0;
                    mkz(m)*J_r(m) -mkz0(p,m)*H_r(m)       jwmu*J_r_d(m)    -jwmu0*H_r_d(m);
                           0                 0                 J_r(m)            -H_r(m);
                     -jwep*J_r_d(m)    jwep0*H_r_d(m)   mkz(m)*J_r(m)    -mkz0(m)*H_r_d(m)];
        field_para(1, m, :) = Lambda \ conj(squeeze(Beta(1,:,m))');
    end
    
else %num_c > 1
    
    %initialize para
    field_para = zeros(num_c, length(order), 4);
    Beta = zeros(num_c,4,length(order));
    LaBe = zeros(num_c,4,length(order));
    LaOm = zeros(num_c,length(order),4,2);
    phaseM = zeros(num_c,num_c,length(order));
    phaseN = zeros(num_c,num_c,length(order));
    % calculate incident beta vectors
    for p = 1:num_c
        Beta(p,:,:) = calc_incident_matrix(para, pos_s, pos_c(p,:), r_c(p), kz);
    end
    
    % coordinates of cylinder p in q
    for p = 1:num_c
        for q = 1:num_c
            [phi_c(p,q),rho_c(p,q)] = cart2pol(pos_c(p,1)-pos_c(q,1),pos_c(p,2)-pos_c(q,2)); %Polar coordinate p in q
        end
    end
    
    % coordinate of source in p
    for p = 1:num_c
        [phi_s(p) rho_s(p)] = cart2pol(pos_s(1)-pos_c(p,1), pos_s(2)-pos_c(p,2)); %Line coordinate in p
    end
    
    % phase parameter (eq. 5.21)
    for p = 1:num_c
        for q = 1:num_c
            phaseM(p,q,:) = exp( 1i*order*(phi_s(p)-phi_c(p,q)) );
            phaseN(p,q,:) = exp( 1i*order*(phi_c(p,q)-phi_s(q)) );
        end
    end
    
    % pre-calculation
    for p = 1:num_c
        H_r(p,:) = besselh(order, 2, k0_rho*r_c(p));
        H_r_d(p,:) = besselhd(order,2,k0_rho*r_c(p));
        J_r(p,:) = besselj(order,k_rho*r_c(p));
        J_r_d(p,:) = besseljd(order,k_rho*r_c(p));
        J0_r(p,:) = besselj(order,k0_rho*r_c(p));
        J0_r_d(p,:) = besseljd(order,k0_rho*r_c(p));
    end
    
    for p = 1:num_c
        for q = 1:num_c
            if (p == q)
                continue;
            end
            H_rpq(p,q,:) = besselh(order_ex,2,k0_rho*rho_c(p,q));
        end
    end
    
    for p = 1:num_c
        mkz(p,:) = order*kz/(k_rho^2*r_c(p));
        mkz0(p,:) = order*kz/(k0_rho^2*r_c(p));
    end
    jwmu = 1i*w*mu/k_rho;
    jwmu0 = 1i*w*mu0/k0_rho;
    jwep = 1i*w*ep/k_rho;
    jwep0 = 1i*w*ep0/k0_rho;
    
    for p = 1:num_c
        for m = 1:length(order)
            Lambda = [         J_r(p,m)           -H_r(p,m)            0                    0;
                      mkz(p,m)*J_r(p,m) -mkz0(p,m)*H_r(p,m)       jwmu*J_r_d(p,m)    -jwmu0*H_r_d(p,m);
                               0                   0                   J_r(p,m)            -H_r(p,m);
                         -jwep*J_r_d(p,m)    jwep0*H_r_d(p,m) mkz(p,m)*J_r(p,m)  -mkz0(p,m)*H_r(p,m)];
            Lambda_inv = inv(Lambda);
            LambdaInvM(p,m,:,:) = Lambda_inv;
            LaBe(p, :, m) = Lambda \ conj(squeeze(Beta(p,:,m)')); %LaBe(p, idxPara, idxOrder)
            
            Omega = [          J0_r(p,m)                 0;
                mkz0(p,m)*J0_r(p,m)           jwmu0*J0_r_d(p,m);
                0                        J0_r(p,m);
                -jwep0*J0_r_d(p,m)     mkz0(p,m)*J0_r(p,m)];
            OmegaM(p,m,:,:) = Omega;
            LaOm(p,m,:,:) = Lambda \ Omega;
            
            for q = 1 : num_c
                if (p == q)
                    continue;
                end
                for n = 1 : length(order)
                    Vmnpq(p,q,m,n) = H_rpq(p,q,order(n)-order(m)-order_ex(1)+1)*phaseM(p,q,m)*phaseN(p,q,n);
                end
                
            end
        end
    end
    
    
    %Iterative solution of scattering matrix
    for idd = 1:ite
        para_old = field_para;
        for p = 1:num_c
            idxQ = [1:p-1 p+1:num_c];
            for m = 1:length(order)
                
                mut_q = Vmnpq(p,idxQ,m,:);
                mutual_qe = squeeze( para_old(idxQ,:,2) );
                mutual_qh = squeeze( para_old(idxQ,:,4) );
                
                mutual_field = [sum((mutual_qe(:)*LaOm(p,m,1,1)+mutual_qh(:)*LaOm(p,m,1,2)).*mut_q(:));
                    sum((mutual_qe(:)*LaOm(p,m,2,1)+mutual_qh(:)*LaOm(p,m,2,2)).*mut_q(:));
                    sum((mutual_qe(:)*LaOm(p,m,3,1)+mutual_qh(:)*LaOm(p,m,3,2)).*mut_q(:));
                    sum((mutual_qe(:)*LaOm(p,m,4,1)+mutual_qh(:)*LaOm(p,m,4,2)).*mut_q(:));];
                
                val = squeeze(LaBe(p,:,m)) +  conj(mutual_field');
                %                 val = conj(squeeze(LaBe(p,:,m))') +  squeeze(LambdaM(p,m,:,:)) \ mutual_field;
                field_para(p,m,:) = val;
                %                 if (p > 1 && m == 100)
                %                     keyboard;
                %                 end
            end
            para_old = field_para;
        end
    end
    
    field_para = para_old;
end
end
