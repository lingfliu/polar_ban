function y = besselhd3(order,m,x)
	y = (besselh(order-3,m,x) - 3*besselh(order-1,m,x) + 3*besselh(order+1,m,x) - besselh(order+3,m,x))/8;
end