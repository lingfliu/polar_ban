function d = calc_dist(a,b)
%Function to calculate euclid distance of two vectors
d = sqrt(sum((a-b).^2));