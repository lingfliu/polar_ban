function [kz dA dBdA ] = get_kz( para,ell,nkz,kzmax)
%GET_KZ get the integral path of kz
%[kz dA dBdA ] = get_kz( paraEM,ell,nkz,kzmax) returns the kz integration
%contour with given settings:
%paraEM: the EM parameters
%ell: the height factor of the paranola to avoid the singularity at k0
%nkz: the number of kz points
%kzmax: the max integration range of kz
%
%See also: simu, get_para, solve_field,  get_field_int

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This file is part of BAN_SIMU.
% 
%BAN_SIMU is free software: you can redistribute it and/or modify
%it under the terms of the GNU General Public License as published by
%the Free Software Foundation, either version 3 of the License, or
%(at your option) any later version.
% 
%BAN_SIMU is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.
% 
%You should have received a copy of the GNU General Public License
%along with BAN_SIMU.  If not, see <http://www.gnu.org/licenses/>.
%
%Composed by: 
%Lingfeng Liu, EMIC/ELEC/UCL, Belgium, lingfeng.liu@uclouvain.be
%Farshad Keshmiri, TELE/ELEC/UCL, Belgium, farshad.keshmiri@uclouvain.be
%First version: 13-10-2009
%Modified: ---
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Rad=para.k0;

a = ([1:nkz]-0.5)/nkz*kzmax;

[p,i]=find(abs(a)<Rad*2);

b=zeros(size(a)); 
% b(i)=sqrt(Rad^2-(a(i)-Rad).^2)/ell;
b(i)=(Rad^2-(a(i)-Rad).^2)/ell/Rad^2;
kz=a+j*b;
dBdA=zeros(size(kz));
% dBdA(i) = -(a(i)-Rad)./sqrt(Rad^2-(a(i)-Rad).^2)/ell;
dBdA(i) = -2*(a(i)-Rad)/ell/Rad^2;

kzN = -a(end:-1:1)+j*b(end:-1:1);
kz = [kzN kz];
dBdA = [dBdA(end:-1:1) dBdA];
dA = kzmax/nkz;