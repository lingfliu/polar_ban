function [E_z E_phi E_rho] = calc_incident_field_ana(para, dimen)
%GET_INCIDENT_FIELD analytical incident field given source
%polarization and observation range
%[Ez Hz] = solve_incident_field(paraGeo,paraEM,paraAlg,range) returns the
%incident E fields and H fields by the given observation range
%
%See also: get_field, get_range

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This file is part of BAN_SIMU.
%
%BAN_SIMU is free software: you can redistribute it and/or modify
%it under the terms of the GNU General Public License as published by
%the Free Software Foundation, either version 3 of the License, or
%(at your option) any later version.
%
%BAN_SIMU is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.
%
%You should have received a copy of the GNU General Public License
%along with BAN_SIMU.  If not, see <http://www.gnu.org/licenses/>.
%
%Composed by:
%Lingfeng Liu, EMIC/ELEC/UCL, Belgium, lingfeng.liu@uclouvain.be
%Farshad Keshmiri, TELE/ELEC/UCL, Belgium, farshad.keshmiri@uclouvain.be
%First version: 13-10-2009
%Modified: ---
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%set parameters
% f = para.f;
w = para.w;
ep0 = para.ep0;
mu0 = para.mu0;
k0 = para.k0;
etta = sqrt(mu0/ep0); %Free space wave impedence

pos_s = para.pos_s;
pos_c = para.pos_c;
pos_s = para.pos_s;
r_c = para.r_c;
num_c = length(para.r_c);
    
x = dimen.x;
y = dimen.y;
z = dimen.z;

I = para.I;

I_e = sqrt(sum(I.^2));

dir_I = I/sqrt(sum(I.^2));


for m = 1:length(x)
    pos = [x(m) y(m) z(m)];
    pos_relativ = pos - pos_s;
    r = sqrt(sum(pos_relativ.^2));
    r_p = sqrt(sum(pos(1:2).^2));
    dir_relativ = pos_relativ / r;
                
    dir_rho = [pos(1) pos(2) 0];
    dir_rho = dir_rho / sqrt(sum(dir_rho.^2));    
    dir_phi = [-pos(2) pos(1) 0];
    dir_phi = dir_phi / sqrt(sum(dir_phi.^2));
    dir_z = [ 0 0 1];
             
    cosTheta = sum(dir_relativ.*dir_I);        
    sinTheta = sqrt(1-cosTheta^2);
    
    dir_Rho = dir_relativ;
     
    dir_Theta = cosTheta*dir_relativ - dir_I;
    if(sum(dir_Theta.^2)>0)        
        dir_Theta = dir_Theta/sqrt(sum(dir_Theta.^2));
    end
        
    if( r > 0)
        E_Rho = I_e/(2*pi)*exp(-j*k0*r)*(etta/r^2+1/(j*w*ep0*r^3) )*cosTheta; % ERho
        E_Theta = I_e/(4*pi)*exp(-j*k0*r)*( j*w*mu0/r+etta/r^2+1/(j*w*ep0*r^3) )*sinTheta; % ETheta
        E_all = E_Rho*dir_Rho + E_Theta*dir_Theta;
    else
        E_Rho = 0;
        E_Theta = 0;
        E_all = [0 0 0];
    end
    
    if (r_p > 0)
        E_z(m) = sum(E_all.*dir_z);
        E_phi(m) = sum(E_all.*dir_phi);
        E_rho(m) = sum(E_all.*dir_rho);
    else        
        E_z(m) = sum(E_all.*dir_z);
        E_phi(m) = 0;
        E_rho(m) = 0;
    end
    
    if num_c == 1
        d_c = sqrt(sum((pos(1:2) - pos_c).^2));
        if (d_c < r_c)
            E_z(m) = 0;
            E_phi(m) = 0;
            E_rho(m) = 0;
        end
    else
        isInside = -1;
        for m = 1:num_c
            d_c(m) = sqrt(sum((pos(1:2) - pos_c(m,:)).^2));          
            if d_c(m) < r_c(m)
                isInside = 1;
                break;
            end
        end
        
        if isInside > 0
            E_z(m) = 0;
            E_phi(m) = 0;
            E_rho(m) = 0;
        end
    end
end   