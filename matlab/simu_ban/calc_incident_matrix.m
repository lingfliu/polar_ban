function Beta = calc_incident_matrix(para, pos_s, pos_c, r_c, kz)
%CALC_BETA calculate the incident matrix (at surface of cylinder, r = r_c)
%   Input: idx, index of the coordinate original
%          m, order

I = para.I;

%Electricity current projection along phi and rho direction
I_vec = I(1:2);
vec_rho = [pos_s(1)-pos_c(1) pos_s(2)-pos_c(2)];
vec_phi = [-vec_rho(2) vec_rho(1)];

I_phi = (I_vec(1)*vec_phi(1)+I_vec(2)*vec_phi(2))/sqrt(vec_phi(1)^2+vec_phi(2)^2);
I_rho = (I_vec(1)*vec_rho(1)+I_vec(2)*vec_rho(2))/sqrt(vec_rho(1)^2+vec_rho(2)^2);

I_z = I(3);

%set parameters
% f = para.f;
w = para.w;
% I = para.I;
ep0 = para.ep0;
mu0 = para.mu0;
ep = para.ep;
mu = para.mu;

k0 = para.k0;
k = para.k;

[k0_rho k_rho] = calc_k(k0,k,kz);

[phi_sc d_sc] = cart2pol(pos_s(1) - pos_c(1), pos_s(2) - pos_c(2)); %distance between source and origin idx 

order = -para.order:para.order;

Beta = zeros(4,length(order));

%z-polarization
if (I_z ~= 0)    
	Cm_z = -I_z*k0_rho^2/(4*w*ep0*d_sc);	

	beta_z = [                   Cm_z*besselh(order,2,k0_rho*d_sc).*besselj(order, k0_rho*r_c);
			   kz/(k0_rho^2*r_c)*Cm_z*order.*besselh(order,2,k0_rho*d_sc).*besselj(order, k0_rho*r_c);
	                             zeros(size(order));
	             -j*w*ep0/k0_rho*Cm_z*besselh(order,2,k0_rho*d_sc).*besseljd(order,k0_rho*r_c)];
   
    Beta = Beta + beta_z;
end

%phi-polarization
if (I_phi ~= 0)
	Cm_phi_e = -I_phi*order*kz/(4*w*ep0*d_sc).*besselh(order,2,k0_rho*d_sc);
	Cm_phi_h = -I_phi*k0_rho/(4*j)*besselhd(order, 2, k0_rho*d_sc);
    mkz0 = order*kz/(k0_rho^2*r_c);
    jwmu0 = j*w*mu0/k0_rho;
    jwep0 = j*w*ep0/k0_rho;
    
	beta_phi = [ Cm_phi_e.*besselj(order,k0_rho*r_c);
	             mkz0.*Cm_phi_e.*besselj(order,k0_rho*r_c) + jwmu0*Cm_phi_h.*besseljd(order,k0_rho*r_c);
	             Cm_phi_h.*besselj(order,k0_rho*r_c);
	             mkz0.*Cm_phi_h.*besselj(order,k0_rho*r_c) - jwep0*Cm_phi_e.*besseljd(order,k0_rho*r_c)];
                 
    Beta = Beta + beta_phi;
end

%rho-polarization
if (I_rho ~= 0)

	J = besselj(order, k0_rho*r_c);
	Jd = besseljd(order, k0_rho*r_c);
	Jdd = besseljd2(order, k0_rho*r_c);

	Hds = besselhd(order, 2, k0_rho*d_sc);
	Hs = besselh(order, 2, k0_rho*d_sc);

	Qm=Jdd*k0_rho + Jd/r_c - order.^2.*J/k0_rho/r_c^2;

	jkz = j*kz/(4*w*ep0);
	mkrho = order/(4*k0_rho*d_sc);
	jm = j*order/(4*r_c*w*ep0);
	jwum = j*w*mu0*order/(4*k0_rho);
   
	EzRho = I_rho*jkz*Qm.*Hds;
	HzRho = I_rho*mkrho.*Qm.*Hs;

	EphiRho = -I_rho*(jm.*Qm.*Hds + jwum.*(Hds.*J/r_c + Hs.*Jd/d_sc));
    
	HphiRho = -I_rho*kz/4*(order.^2.*Hs.*J / k0_rho^2/r_c/d_sc + Hds.*Jd);

	beta_rho = [EzRho;
                EphiRho;
				HzRho;				
				HphiRho];

	Beta = Beta + beta_rho;
end

end

