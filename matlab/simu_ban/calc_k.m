function [k0_rho k_rho ] = calc_k( k0, k, kz )

k0_rho = sqrt(k0^2 - kz.^2);
k0_rho = real(k0_rho) - 1i*abs(imag(k0_rho));

k_rho = sqrt(k^2 - kz.^2);
k_rho = real(k_rho) - 1i*abs(imag(k_rho));

end

