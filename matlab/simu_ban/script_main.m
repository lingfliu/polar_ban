clear; clc;
warning off all;
% Main script of simu_ban

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This codec are developped under privilage copyrights.
% Authored by Lingfeng LIU, +86-186-7982-3932, lingf.liu@gmail.com
% Last edited by Lingfeng LIU
% Version 5.4
% Users are not allowed to copy, distribute, or modify the codec without prior authorization of the author.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%S.B. static field calculation 

%%%%%%%%%%%%%%%%%%%%%%
%EM parameters
%%%%%%%%%%%%%%%%%%%%%%
para.f = 2.45e9; %freq.
para.w = 2*pi*para.f; % rad.
para.c0 = 299792458; %wavespeed
para.lambda0 = para.c0 / para.f; %free-space wavelength 

para.I = 1e-10*[1 0 0]; %current intensity vector along [x y z]

para.mu0 = 4*pi*1e-7; %free-space permeability 
para.ep0 = 1/para.c0^2/para.mu0; %free-space permitivity 
para.k0 = sqrt(para.w^2*para.mu0*para.ep0); %free-space wavenumber 

tissue = 19; %skin tissue 
[ep_r sigma_r] = cole_cole('value',19,para.f); %get the EM paras by Cole-Cole equation
para.ep = (para.ep0*ep_r+1i*(sigma_r/para.w)); %tissue permitivity 
para.mu = para.mu0; %tissue permeability 
para.k = sqrt(para.w^2*para.mu*para.ep); %tissue wavenumber 

%generate kz integration
[kz dA dBdA] = get_kz(para,200,400,10*para.k0); 
para.kz = kz;
para.dA = dA;
para.dBdA = dBdA;

%%%%%%%%%%%%%%%%%%%%%%
%algorithm settings
%%%%%%%%%%%%%%%%%%%%%%
para.order = 120; %order of harmonics (2*order for mutual scattering problem)
para.iter = 10; %iterations of mutual scattering solution

%%%%%%%%%%%%%%%%%%%%%%
%geometry
%%%%%%%%%%%%%%%%%%%%%%
para.pos_s = [0 0.55 0];

% para.pos_c = [0 0; -0.8 0; 0.8 0]; %cylinder central coordinate (X-Y plane)
% para.r_c = [0.5; 0.15; 0.15]; %cylinder radius

para.pos_c = [0 0];
para.r_c = [0.5];
%%%%%%%%%%%%%%%%%%%%%%
%computation dimension
%%%%%%%%%%%%%%%%%%%%%%
% [x, y] = meshgrid(-1:0.02:1 -1:0.02:1); %computation range
% z = zeros(size(x));

% [a z] = meshgrid([0:360]/180*pi, -0.5:0.02:0.5);
% x = 0.5*cos(a);
% y = 0.5*sin(a);

% z = -1:0.01:1;
% x = zeros(size(z));
% y = 0.55*ones(size(z));

x = 0.55*cos([0:360]/180*pi);
y = 0.55*sin([0:360]/180*pi);
z = zeros(size(x));

dimen.x = x(:);
dimen.y = y(:);
dimen.z = z(:);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%field parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
num_c = length(para.r_c);
num_kz = length(para.kz);
order = -para.order:para.order;
field_para = zeros(num_kz, num_c, length(order), 4); %[idxKz, idx_cylinder, order, a,b,c,d]

%%%%%%%%%%%%%%%%%%%%%%
%solve field
%%%%%%%%%%%%%%%%%%%%%%
for idx_kz = 1:length(para.kz)
    fprintf(sprintf('solving field percent = %.2f\n',idx_kz/length(para.kz)*100));    
    field_para(idx_kz, : , :, :) = solve_field(para, para.kz(idx_kz));   
end

%%
%%%%%%%%%%%%%%%%%%%%%%
%compute field
%%%%%%%%%%%%%%%%%%%%%%
tic
[E_s_z E_s_phi E_s_rho H_s_z H_s_phi H_s_rho] = calc_scatter_field(para, field_para, dimen);
toc

%%%%%%%%%%%%%%%%%%%%%%
%plot field
%%%%%%%%%%%%%%%%%%%%%%
%%
tic
[E_i_z E_i_phi E_i_rho] = calc_incident_field_ana(para, dimen);
% [E_i_z E_i_rho E_i_phi E_tot] = calc_incident_field_ana_old(para, dimen);
toc

keyboard
%%

if num_c > 1
    E_s_z = sum(E_s_z,2);
    E_s_phi = sum(E_s_phi,2);
    E_s_rho = sum(E_s_phi,2);        
end

for m = 1:length(x(:))
    E_s_vec(m,:) = [E_s_z(m) E_s_phi(m) E_s_rho(m)];
    E_i_vec(m,:) = [E_i_z(m) E_i_phi(m) E_i_rho(m)];
    E_t_vec(m,:) = E_s_vec(m,:) + E_i_vec(m,:);
    
end

%%
for m = 1:length(x(:))   
    E_i_vec(m,:) = [E_i_z(m) E_i_phi(m) E_i_rho(m)];       
end
for m = 1:10:length(x(:))   
    vec = abs(E_i_vec(m,:));
    vec_rho = [x(m) y(m)];
    vec_rho = vec_rho / sqrt(sum(vec_rho.^2));
    vec_phi = [-y(m) x(m)];
    vec_phi = vec_phi / sqrt(sum(vec_phi.^2));
    
    x_vec = [1 0];
    y_vec = [0 1];
    
    E_z = vec(1);
    E_x = vec(2)*sum(vec_phi.*x_vec) + vec(3)*sum(vec_rho.*x_vec);
    E_y = vec(2)*sum(vec_phi.*y_vec) + vec(3)*sum(vec_rho.*y_vec);    
    
    vec = [E_x E_y E_z];
    vec = vec / sqrt(sum(vec.^2));
    x_coor = [x(m) x(m)+vec(1)];
    y_coor = [y(m) y(m)+vec(2)];
    z_coor = [z(m) z(m)+vec(3)];
    
    plot3(x_coor, y_coor, z_coor);
    hold on    
end