function [e_real,sigma] = cole_cole(select,tissue,freq)
%FOUR_COLECOLE, computation of dieletric properties for human tissue
%[e_real,sigma] = four_colecole('value',tissue,freq) returns the value of
%real epsilon and sigma with given tissue index (1-26) and frequency f. For
%default choice, tissue=19 for dry skin
%
%See also: get_para


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%This file is part of BAN_SIMU.
% 
%BAN_SIMU is free software: you can redistribute it and/or modify
%it under the terms of the GNU General Public License as published by
%the Free Software Foundation, either version 3 of the License, or
%(at your option) any later version.
% 
%BAN_SIMU is distributed in the hope that it will be useful,
%but WITHOUT ANY WARRANTY; without even the implied warranty of
%MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%GNU General Public License for more details.
% 
%You should have received a copy of the GNU General Public License
%along with BAN_SIMU.  If not, see <http://www.gnu.org/licenses/>.
%
%Composed by: 
%Lingfeng Liu, EMIC/ELEC/UCL, Belgium, lingfeng.liu@uclouvain.be
%Farshad Keshmiri, TELE/ELEC/UCL, Belgium, farshad.keshmiri@uclouvain.be
%First version: 13-10-2009
%Modified: ---
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

switch select
    case 'plot'
        e=1;s=1;z=1;v=1;
        
        for a=1:1001
            
            [e,s] =FourColeCole('value',tissue,3e9+(a-1)*(7e6));
            x(a)=e;
        end
        
        
        freq = 3e9:7e6:10e9;
        plot(freq,real(x))
        hold on
        plot(freq,-imag(x))
        
        
        
        
    case 'value'
        c0 = 299792458;
        m_0 = 4*pi*1e-7;
        e_0 = 1/c0^2/m_0;
        %c= 1 /sqrt(e_0*m_0);
        omega = 2*pi*freq ;
        a= tissue;

        info = [4 40 8.842 0.1 50 3.183 0.1 0.25 1e5 159.155 0.2 1e7 1.592 0 % 1 Aorta 
            2.5 16 8.842 0.1 400 159.155 0.1 0.2 1e5 159.155 0.2 1e7 15.915 0 % 2 Bladder
            4 56 8.377 0.1 5200 132.629 0.1 0.7 0 159.155 0.2 0 15.915 0 % 3 Blood
            2.5 18 13.263 0.22 300 79.577 0.25 0.07 2e4 159.155 0.2 2e7 15.915 0 % 4 Bone
            2.5 10 13.263 0.2 180 79.577 0.2 0.02 5e3 159.155 0.2 1e5 15.915 0 % 5 Bone
            4 45 7.958 0.1 400 15.915 0.150 0.02 2e5 106.103 0.22 4.5e7 5.305 0 % 6 Brain
            2.5 3 17.680 0.1 15 63.66 0.1 0.01 5e4 454.7 0.1 2e7 13.260 0 % 7 Breast fat
            4 38 13.263 0.150 2500 144.686 0.15 0.15 1e5 318.310 0.1 4e7 15.915 0 % 8 Cartilage
            4 65 7.958 0.1 40 1.592 0 2 0 159.155 0 0 15.915 0  % 9 Cerebro spinal fluid
            4 48 7.958 0.1 4000 159.155 0.05 0.4 1e5 15.915 0.2 4e7 15.915 0 % 10 Cornea
            4 50 7.958 0.1 4000 159.155 0.1 0.5 1e5 159.155 0.2 5e6 15.915 0 % 11 Eye tissu
            2.5 9 7.958 0.2 35 15.915 0.1 0.035 3.3e4 159.155 0.05 1e7 15.915 0.010 % 12 Fat
            4 66 7.579 0.05 50 1.592 0 1.4 0 159.155 0.2 0 15.915 0.2 % 13 Gall bladder bile
            4 50 7.958 0.1 1200 159.155 0.050 0.050 4.50e5 72.343 0.220 2.50e7 4.547 0 % 14 Heart
            4 47 7.958 0.1 3500 198.944 0.220 0.050 2.50E5 79.577 0.220 3e7 4.547 0 % 15 Kidney
            4 39 8.842 0.1 6000 520.516 0.2 0.020 5e4 22.736 0.2 3e7 15.915 0.050 % 16 Liver
            2.5 18 7.958 0.1 500 63.662 0.1 0.030 2.50e5 159.155 0.2 4e7 7.958 0 % 17 Lung
            4 50 7.234 0.1 7000 353.678 0.1 0.2 1.2e6 318.310 0.1 2.50e7 2.274 0 % 18 Muscle
            4 32 7.234 0 1100 32.481 0.2 0 0 159.155 0.2 0 15.915 0.2 % 19 Skin (dry)
            4 39 7.958 0.1 280 79.577 0 0 3e4 1.592 0.16 3e4 1.592 0.2 % 20 Skin (wet)
            4 50 7.958 0.1 10000 159.155 0.1 0.5 5e5 159.155 0.2 4e7 15.915 0 % 21 Small instestine
            4 60 7.958 0.1 2000 79.577 0.1 0.5 1e5 159.155 0.2 4e7 15.915 0 % 22 Stomach
            4 55 7.958 0.1 5000 159.155 0.1 0.4 1e5 159.155 0.2 4e7 15.915 0 % 23 Testis
            4 50 7.958 0.1 4000 159.155 0.1 0.25 1e5 159.155 0.2 4e7 15.915 0 % 24 Tongue
            2.500 	3.00 	7.958 	0.200 	15 	15.915 	0.100 	0.010 	3.30E+4 	159.155 	0.050 	1.00E+7 	7.958 	0.010  % 25 fat 
            2.500 	9.00 	7.958 	0.200 	35 	15.915 	0.100 	0.035 	3.30E+4 	159.155 	0.050 	1.00E+7 	15.915 	0.010  % 26 fat 2 
        ];

        A = info(a,1);
        B = info(a,2)/(1+(j*omega*info(a,3)*1e-12)^(1-info(a,4)));
        
        C = info(a,5)/(1+(j*omega*info(a,6)*1e-9)^(1-info(a,7)));
        
        D = info(a,9)/(1+(j*omega*info(a,10)*1e-6)^(1-info(a,11)));
        
        E = info(a,12)/(1+(j*omega*info(a,13)*1e-3)^(1-info(a,14)));
        
        F = info(a,8)/(j*omega*e_0);
        e = A+B+C+D+E+F;
    
        
        sigma = imag(-e*omega*e_0);
        e_real = real(e) ;
        

end
