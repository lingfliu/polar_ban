function [E_z E_phi E_rho H_z H_phi H_rho]= calc_incident_field(em_para, geo_para, alg_para, para)

%solve the parameters at kz for all cylinders
%return: a_m, b_m, c_m, d_m of each cylinder

%set parameters
f = em_para.f;
w = em_para.w;
I = em_para.I;
ep0 = em_para.ep0;
mu0 = em_para.mu0;
% ep = em_para.ep;
% mu = em_para.mu;

k0 = em_para.k0;
k = em_para.k;
kz = em_para.kz;
[k0_rho k_rho] = calc_k(k0, k, kz);

% k = em_para.k;
% k_rho = sqrt(em_para.k^2 - kz.^2);
dA = em_para.dA;
dBdA = em_para.dBdA;
phase_int = 1/(2*pi)*(1+1i*dBdA).*dA;

order = -alg_para.order:alg_para.order;

pos_c = geo_para.pos_cylinder;
r_c = geo_para.r_cylinder;
num_cylinder = length(r_c);

pos_s = geo_para.pos_src;
[phi_s rho_s] = cart2pol(pos_s(1)-pos_c(1,1), pos_s(2)-pos_c(1,2));

%Electricity current projection along phi and rho direction
I_vec = I(2:3);
vec_rho = [pos_s(1)-pos_c(1,1) pos_s(2)-pos_c(1,2)];
vec_phi = [-vec_rho(2) vec_rho(1)];

I_phi = (I_vec(1)*vec_phi(1)+I_vec(2)*vec_phi(2))/sqrt(vec_phi(1)^2+vec_phi(2)^2);
I_rho = (I_vec(1)*vec_rho(1)+I_vec(2)*vec_rho(2))/sqrt(vec_rho(1)^2+vec_rho(2)^2);

I_z = I(1);


x = geo_para.x;
y = geo_para.y;
z = geo_para.z;

E_z_k = zeros(length(x), length(kz));
E_phi_k = zeros(length(x), length(kz));
E_rho_k = zeros(length(x), length(kz));
H_z_k = zeros(length(x), length(kz));
H_phi_k = zeros(length(x), length(kz));
H_rho_k = zeros(length(x), length(kz));

E_z = zeros(1,length(x));
E_phi = zeros(1,length(x));
E_rho = zeros(1,length(x));
H_z = zeros(1,length(x));
H_phi = zeros(1,length(x));
H_rho = zeros(1,length(x));

for km = 1:length(kz)
    fprintf(sprintf('incident field calculation percent = %.2f\n',km/length(kz)*100));
    
    Hs = besselh(order,2,rho_s*k0_rho(km));
    Hds = besselhd(order,2,rho_s*k0_rho(km));
    Js = besselj(order,rho_s*k0_rho(km));
    Jds = besseljd(order,rho_s*k0_rho(km));
    
    %incident paras
    if I_z~=0
        Cm_z = -I_z*k0_rho(km)^2/(4*w*ep0);
        
        Ue_z = Cm_z*Hs;
        Ve_z = Cm_z*Js;
        Uh_z = zeros(size(Ve_z));
        Vh_z = zeros(size(Ve_z));
    end
    
    if I_phi~=0
        Cm_phi = -I_phi*order*kz(km)/ (4*w*ep0*rho_s);
        Dm_phi = -I_phi*k0_rho(km)/(4*1i);
        Ue_phi = Cm_phi.*Hs;
        Ve_phi = Cm_phi.*Js;
        Uh_phi = Dm_phi*Hds;
        Vh_phi = Dm_phi*Jds;
    end
    
    if I_rho~=0
        Hdds = besselhd2(order,2,rho_s*k0_rho(km));
        Jdds = besseljd2(order,rho_s*k0_rho(km));
        
        Cm_rho = 1i*kz/(4*w*ep0);
        
        jkz = 1i*kz(km)/(4*w*ep0); %for ez
        mkrho = order/(4*k0_rho(km)*r_c(1)); %for ez
        jm = 1i*order/(4*w*ep0); %for ephi
        jwum = 1i*w*mu0*order/(4*k0_rho(km)); %for ephi
        jwe = 1/(4*1i*w*ep0); %for erho
        wu = w*mu0/4; %for erho
        
        mrhos = order/(4*k0_rho(km)*rho_s); %for hz
        m2rhos = order.^2/(k0_rho(km)^2*rho_s);
    end
    
    for idx = 1:length(x)
%         E_z_k(idx,km) = 0;
%         E_phi_k(idx,km) = 0;
%         E_rho_k(idx,km) = 0;
%         H_z_k(idx,km) = 0;
%         H_phi_k(idx,km) = 0;
%         H_rho_k(idx,km) = 0;
        
        is_inside = -1;
        for p = 1:num_cylinder
            [phi(p) rho(p)] = cart2pol(x(idx)-pos_c(p,1), y(idx)-pos_c(p,2));
            if (rho(p) < r_c(p))
                is_inside = 1;
                break;
            end
        end
        
        
        %Calculate incident field only when the point is outside the cylinders
        if (is_inside < 0)
            rho_point = rho(1);
            phi_point = phi(1);
            
            zm = z(idx);
            
            %precalculation
            phase = exp(1i*order*(phi_point-phi_s));
            
            mkz = order*kz(km)/rho_point/k0_rho(km);
            jwmu0 = 1i*w*mu0/k0_rho(km);
            jwep0 = 1i*w*ep0/k0_rho(km);
            wep0m = w*mu0*order/rho_point;
            
            if (rho_point > rho_s)
                H = besselh(order,2,rho_point*k0_rho(km)).*phase;
                Hd = besselhd(order,2,rho_point*k0_rho(km)).*phase;
            else
                J = besselj(order,rho_point*k0_rho(km)).*phase;
                Jd = besseljd(order,rho_point*k0_rho(km)).*phase;
            end
            
            if (I_z ~= 0)
                if (rho_point > rho_s)
                    %E_z
                    E_z_k(idx, km) = E_z_k(idx,km) + sum(Ve_z.*H);
                    
                    %E_phi_k
                    E_phi_k(idx, km) = E_phi_k(idx, km) + sum(mkz.*Ve_z.*H);
                    
                    %E_rho_k
                    E_rho_k(idx, km) = E_rho_k(idx, km) + sum(Ve_z.*Hd)*(-1i*kz(km)/k0_rho(km));
                    
                    %H_z_k = 0
                    
                    %H_phi_k
                    H_phi_k(idx, km) = H_phi_k(idx, km) + sum(Ve_z.*mkz.*Hd)/k0_rho(km)^2;
                    
                    %H_rho_k
                    H_rho_k(idx, km) = H_rho_k(idx, km) + sum(Ve_z.*H)*(-1i*kz(km)/k0_rho(km));
                else
                    %E_z
                    E_z_k(idx, km) = E_z_k(km) + sum(Ue_z.*J);
                    
                    %E_phi_k
                    E_phi_k(idx, km) = E_phi_k(km) + sum(mkz.*Ue_z.*J)/k0_rho(km)^2;
                    
                    %E_rho_k
                    E_rho_k(idx, km) = E_rho_k(km) + sum(-j*kz(km)*Ue_z.*Jd)/k0_rho(km);
                    
                    %H_z_k = 0
                    
                    %H_phi_k
                    H_phi_k(idx, km) = H_phi_k(idx, km) + sum(Ue_z.*mkz.*Jd)/k0_rho(km)^2;
                    
                    %H_rho_k
                    H_rho_k(idx, km) = H_rho_k(idx, km) + sum(Ue_z.*J)*(-1i*kz(km)/k0_rho(km));
                end
            end
            
            if (I_phi ~= 0)
                if (rho_point > rho_s)
                    %E_z
                    E_z_k(idx, km) = E_z_k(idx,km) + sum(Ve_phi.*H);
                    
                    %E_phi_k
                    E_phi_k(idx, km) = E_phi_k(idx, km) + sum(mkz.*Ve_phi.*H + jwmu0*k0_rho(km)*Vh_phi.*Hd)/k0_rho(km)^2;
                    
                    %E_rho_k
                    E_rho_k(idx, km) = E_rho_k(idx, km) + sum(-1i*kz(km)*Ve_phi.*Hd+w*mu0/rho_point*order.*Vh_phi.*H)/k0_rho(km);
                    
                    %H_z_k
                    H_z_k(idx, km) = H_z_k(idx, km) + sum(Vh_phi.*H);
                    
                    %H_phi_k
                    H_phi_k(idx, km) = H_phi_k(idx, km) + sum(Ve_phi.*mkz.*Hd - jwep0*k0_rho(km)*Vh_phi.*Hd)/k0_rho(km)^2;
                    
                    %H_rho_k
                    H_rho_k(idx, km) = H_rho_k(idx, km) + sum(-1i*kz(km)*k0_rho(km)*Ve_phi.*H - k0_rho(km)*wep0m.*Vh_phi.*Hd)/k0_rho(km)^2;
                else
                    %E_z
                    E_z_k(idx, km) = E_z_k(km) + sum(Ue_phi.*J);
                    
                    %E_phi_k
                    E_phi_k(idx, km) = E_phi_k(km) + sum(mkz.*Ue_phi.*J + jwmu0*k0_rho(km)*Uh_phi.*Jd)/k0_rho(km)^2;
                    
                    %E_rho_k
                    E_rho_k(idx, km) = E_rho_k(km) + sum(-1i*kz(km)*Ue_phi.*Jd + w*mu0/rho_point*order.*Uh_phi.*H)/k0_rho(km);
                    
                    %H_z_k
                    H_z_k(idx, km) = H_z_k(idx, km) + sum(Uh_phi.*J);
                    
                    %H_phi_k
                    H_phi_k(idx, km) = H_phi_k(idx, km) + sum(Ue_phi.*mkz.*Jd - jwep0*k0_rho(km).*Uh_phi.*Jd)/k0_rho(km)^2;
                    
                    %H_rho_k
                    H_rho_k(idx, km) = H_rho_k(idx, km) + sum(-1i*kz(km)*Ue_phi.*J - wep0m.*Uh_phi.*Hd)/k0_rho(km);
                end
            end
            
            if (I_rho ~= 0)
                if (rho_point > rho_s)

                    H = besselh(order,2,rho_point*k0_rho(km)).*phase;
                    Hd = besselhd(order,2,rho_point*k0_rho(km)).*phase;
                    Hdd = besselhd2(order,2,rho_point*k0_rho(km)).*phase;
                    Hddd = besselhd3(order,2,rho_point*k0_rho(km)).*phase;
                                       
                    Qm = Hdd*k0_rho(km)+Hd/rho_point+H.*(order.^2)/(rho_point^2*k0_rho(km));
                    
                    E_z_k(idx,km) = E_z_k(idx,km) + sum(jkz*Qm.*Jds);
                    E_phi_k(idx,km) = E_phi_k(idx,km) +sum(-jm.*Qm.*Jds/rho_point - jwum.*(Hd.*Js/rho_s + H.*Jds/rho_point));
                    E_rho_k(idx,km) = E_rho_k(idx,km) + sum(-jwe*(-Hd/rho_point^2 + k0_rho(km)/rho_point*Hdd+k0_rho(km)^2*Hddd+(order.^2).*H*2/rho_point^2/k0_rho(km) - (order.^2).*Hd/rho_point^2).*Jds - wu*((order.^2).*H.*Js/k0_rho(km)/rho_point/rho_s +Hd.*Jds) );
                    
                    H_z_k(idx,km) = H_z_k(idx,km) + sum(order.*Qm.*Js/(4*k0_rho(km)*rho_s));
                    
                    H_phi_k(idx,km) = H_phi_k(idx,km) + sum(-j*kz(km)/(4*j)*((order.^2).*H.*Js/(k0_rho(km)^2*rho_point*rho_s) + Hd.*Jds));
                    
                    H_rho_k(idx,km) = H_rho_k(idx,km) + sum(j*kz(km)/(4*k0_rho(km))*order.*(Hd.*Js/rho_point) + H.*Jds/rho_s);
                else
                    J = besselj(order,rho_point*k0_rho(km)).*phase;
                    Jd = besseljd(order,rho_point*k0_rho(km)).*phase;
                    Jdd = besseljd2(order,rho_point*k0_rho(km)).*phase;
                    Jddd = besseljd3(order,rho_point*k0_rho(km)).*phase;
                    
                    Qm = Jdd*k0_rho(km)+Jd/rho_point+H.*(order.^2)/(rho_point^2*k0_rho(km));
                    
                    E_z_k(idx,km) = E_z_k(idx,km) + sum(jkz*Qm.*Hds);
                    E_phi_k(idx,km) = E_phi_k(idx,km) +sum(-jm.*Qm.*Hds/rho_point - jwum.*(Jd.*Hs/rho_s + J.*Hds/rho_point));
                    E_rho_k(idx,km) = E_rho_k(idx,km) + sum(-jwe*(-Jd/rho_point^2 + k0_rho(km)/rho_point*Jdd+k0_rho(km)^2*Jddd+(order.^2).*J*2/rho_point^2/k0_rho(km) - (order.^2).*Jd/rho_point^2).*Hds - wu*((order.^2).*Hs.*J/k0_rho(km)/rho_point/rho_s +Hds.*Jd) );
                    
                    H_z_k(idx,km) = H_z_k(idx,km) + sum(order.*Qm.*Hs/(4*k0_rho(km)*rho_s));
                    
                    H_phi_k(idx,km) = H_phi_k(idx,km) + sum(-j*kz(km)/(4*j)*((order.^2).*Hs.*J/(k0_rho(km)^2*rho_point*rho_s) + Hds.*Jd));
                    
                    H_rho_k(idx,km) = H_rho_k(idx,km) + sum(j*kz(km)/(4*k0_rho(km))*order.*(Hds.*J/rho_point) + Hs.*Jd/rho_s);
                end
            end
        else
            %do nothing
        end
        
        E_z_k(idx,km) = E_z_k(idx,km)*exp(-j*kz(km)*zm);
        E_phi_k(idx,km) = E_phi_k(idx,km)*exp(-j*kz(km)*zm);
        E_rho_k(idx,km) = E_rho_k(idx,km)*exp(-j*kz(km)*zm);
        
        H_z_k(idx,km) = H_z_k(idx,km)*exp(-j*kz(km)*zm);
        H_phi_k(idx,km) = H_phi_k(idx,km)*exp(-j*kz(km)*zm);
        H_rho_k(idx,km) = H_rho_k(idx,km)*exp(-j*kz(km)*zm);
    end
end

%Integration
E_z = phase_int*conj(E_z_k');
E_phi = phase_int*conj(E_phi_k');
E_rho = phase_int*conj(E_rho_k');

H_z = phase_int*conj(H_z_k');
H_phi = phase_int*conj(H_phi_k');
H_rho = phase_int*conj(H_rho_k');
end
