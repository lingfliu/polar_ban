function [phi r] = cart2pol(x,y)
    %conver cartisian coordinate into polar coordinate
    %return: [phi r]

    r = sqrt(x^2+y^2);

    if r ==0
        phi = 0;
        return
    end

    if x == 0
        if y<0
            phi = 270*pi/180;
        elseif y>0
            phi = 90*pi/180;
        end
        return
    end

    tmp_phi = atan(y/x);

    if x>=0&&y>=0
        phi = tmp_phi;
    elseif x<0&&y>=0
        phi = tmp_phi+pi;
    elseif x<0&&y<0
        phi = tmp_phi+pi;
    elseif x>=0&&y<0
        phi = tmp_phi+2*pi;
    end