function grid = calc_grid(idx_grid, size_grid, pos_idx)
%通过某方向网格的索引值，给出对应的网格尺寸的大小。
% index_array是索引值数组，size_arrary是对应网格尺寸数组，两个数组一样大小。
%Posindex是要搜索的网格索引值。

if pos_idx>=idx_grid(end)
    grid=size_grid(end);           %若索引值大于等于最后一个值（也是最大的），则网格尺寸按照最后一个尺寸算。
else
    idx = find(idx_grid>pos_idx);
    grid = size_grid(idx(1)-1);    %找出最靠近索引值处的网格尺寸，往小方向取值。例如index_array=【12 15 34】
                                        %对应的size_array=【1 2 3】，Posindex=13，那么结果gridsize=1.
end

end