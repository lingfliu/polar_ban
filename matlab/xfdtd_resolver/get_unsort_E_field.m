function [E_x E_y E_z] = get_unsort_E_field(result_file, type, num_point)

if type == 1 %narrowband, single frequency    
    result_file_E_x_re = [result_file '_steady_Exr_total_freq0.bin'];
    result_file_E_x_im = [result_file '_steady_Exi_total_freq0.bin'];
    result_file_E_y_re = [result_file '_steady_Eyr_total_freq0.bin'];
    result_file_E_y_im = [result_file '_steady_Eyi_total_freq0.bin'];
    result_file_E_z_re = [result_file '_steady_Ezr_total_freq0.bin'];
    result_file_E_z_im = [result_file '_steady_Ezi_total_freq0.bin'];
        
    fid = fopen( result_file_E_x_re );
    E_x_re = fread( fid, double(num_point), 'single' );
    fclose(fid);
    
    fid = fopen( result_file_E_x_im );
    E_x_im = fread( fid, double(num_point), 'single' );
    fclose(fid);
    E_x = E_x_re+ 1i*E_x_im;
    
    fid = fopen( result_file_E_y_re );
    E_y_re = fread( fid, double(num_point), 'single' );
    fclose(fid);
    
    fid = fopen( result_file_E_y_im );
    E_y_im = fread( fid, double(num_point), 'single' );
    fclose(fid);
    E_y = E_y_re+ 1i*E_y_im;    
    
    fid = fopen( result_file_E_z_re );
    E_z_re = fread( fid, double(num_point), 'single' );
    fclose(fid);
    
    fid = fopen( result_file_E_z_im );
    E_z_im = fread( fid, double(num_point), 'single' );
    fclose(fid);
    E_z = E_z_re+ 1i*E_z_im;    
else
    E_x = 0;
    E_y = 0;
    E_z = 0;
end

end