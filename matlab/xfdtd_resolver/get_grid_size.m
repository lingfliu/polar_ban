function [idx size_array] = get_grid_size(geo_file, axis)
%获取某轴（aix只能为'X' 'Y' 'Z'）上的网格划分大小。返回值为两组数，第一组为网格索引，第二组为对应的网格尺寸
%filename 是对应的geometry.input文件，axis只能是'X''Y''Z'的一个。
%index_array对应网格索引数组，例如【23 24 56 124】（这个是根据.input文件确定的
%size_array是对应的网格索引对应的其网格尺寸，如【1e-5 2e-5 1e-5 4e-5】

begin_Del=strrep('begin_<Del*>','*',axis);              %例如替换成'begin_<DelX>'，根据.input文件确定的
end_Del=strrep('end_<Del*>','*',axis);
fid = fopen(geo_file,'rt');
m=1;
while feof(fid) == 0;
    tline=fgetl(fid);
    if strncmp(tline,begin_Del,11)==1               %判断若是begin_Del*行，继续读取后面跟着的数据
       %disp(tline)
       tline=fgetl(fid);                             %开始读取数据
      while strncmp(tline,end_Del,10)==0             %判断是否读到’end_Del*‘，循环读。
                                                     % disp(tline);显示每次读取的行的内容，调试用
          [idx(m),size_array(m)]=GridSize(tline);
          m=m+1;
          tline=fgetl(fid);                          %把每行的字符串转换为两个数据，分别放入对应的数组中。
      end
        break;                                          %读到end_Del*行时，跳出大while，此程序结束。
    end
end
fclose(fid);

end

function [grid_index,grid_size] = GridSize( tline )
%读取geometry.input文件中对应X Y Z方向的不同网格位置的网格大小时将字符串转为数字
% begin_<DelY> 
%18 4.99999987e-05
%类似上面，返回值为： grid_index=18，grid_value=4.999e-05
spaceindex=strfind(tline,' ');
grid_index=str2num(tline(1:spaceindex-1));
grid_size=str2num(tline(spaceindex+1:end));
end