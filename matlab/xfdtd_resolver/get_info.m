function info = get_info(info_file)
fid = fopen(info_file);
info.Rmpt = fread(fid, 4, 'char*1' ); %
info.version = fread( fid, 1, '*uint8', 0, 'a' ); %version

bitmask = fread( fid, 1, '*uint32', 0, 'a' );
info.has_timeDomain_scatteredE = bitget( bitmask, 32 ); % 0
info.has_timeDomain_totalE = bitget( bitmask, 31 ); % 0
info.has_timeDomain_scatteredH = bitget( bitmask, 30 ); % 0
info.has_timeDomain_totalH = bitget( bitmask, 29 ); % 0
info.has_timeDomain_scatteredB = bitget( bitmask, 28 ); % 0
info.has_timeDomain_totalB = bitget( bitmask, 27 ); % 0
info.has_timeDomain_J = bitget( bitmask, 26 ); % 0
info.has_discreteFreq_totalE = bitget( bitmask, 25 ); % 1 if steady state E is requested
info.has_discreteFreq_totalH = bitget( bitmask, 24 ); % 0
info.has_discreteFreq_J = bitget( bitmask, 23 ); % 0
info.has_discreteFreq_totalB = bitget( bitmask, 22 ); % 0

info.num_point = fread( fid, 1, '*uint32', 0, 'a' );%读取这个sensor上有多少个数据点

fclose(fid);
end