clear all;clc;

root_dir = 'D:\xfdtd\';
simu_name = 'irho_3mm_3cm_dryskin.xf\Simulations\000001\Run0001';
sensor_names = ['MultiPoint_plane_z100_1';
    'MultiPoint_plane_z200_2';
    'MultiPoint_plane_z300_3';
    'MultiPoint_plane_z400_4';
    'MultiPoint_plane_z500_5';
    'MultiPoint_plane_z600_6';
    'MultiPoint_plane_z700_7';
    'MultiPoint_plane_z800_8';
    'MultiPoint_plane_z900_9';];

for idx = 1
    idx
    sensor_name = sensor_names(idx,:);
    result_file = fullfile(root_dir, simu_name, 'output', sensor_name);

    info_file = [result_file '_info.bin'];
    geom_file = [result_file '_geom.bin'];
 
    info = get_info(info_file);

    [E_x_unsort E_y_unsort E_z_unsort] = get_unsort_E_field(result_file, 1, info.num_point);

    x_pos = zeros(1,info.num_point);%初始化一个空间
    y_pos = zeros(1,info.num_point);
    z_pos = zeros(1,info.num_point);
    fid = fopen( geom_file );
    for k = 1:info.num_point;
        x_pos(k) = fread( fid, 1, '*uint32' );%可能是 x1 x1 x1 x1 x1 x1  x2 x2 x2 x2 x2 x2  x3 x3....每个'xn'数字个数是包括一个x截面的数据量的个数
        y_pos(k) = fread( fid, 1, '*uint32' );%可能是 y1 y1 y1 y2 y2 y2  y1 y1 y1 y2 y2 y2  y1 y1....每个xn yn zn其实是这个节点对应的gird的索引，不一定从0开始
        z_pos(k) = fread( fid, 1, '*uint32' );%可能是 z1 z2 z2 z1 z2 z3  z1 z2 z3 z1 z2 z3  z1 z2....
    end
    fclose(fid);

    
    % 计算每个轴上划分了多少个网格，并把“网格索引”分别建立成数组
    x_size=abs(x_pos(end)-x_pos(1)+1);      %这个方向的网格的个数，根据实际情况而定 如56-5=51个网格
    y_size=abs(y_pos(end)-y_pos(1)+1);
    z_size=abs(z_pos(end)-z_pos(1)+1);
    
    if x_pos(end) > x_pos(1)
        x_range=(x_pos(1):x_pos(end)); %如【5,6,7.。。56】，参考上
    else
        x_range=(x_pos(end):x_pos(1)); %如【5,6,7.。。56】，参考上
    end
    if y_pos(end) > y_pos(1)
        y_range=(y_pos(1):y_pos(end)); %如【5,6,7.。。56】，参考上
    else
        y_range=(y_pos(end):y_pos(1)); %如【5,6,7.。。56】，参考上
    end
    if z_pos(end) > z_pos(1)
        z_range=(z_pos(1):z_pos(end)); %如【5,6,7.。。56】，参考上
    else
        z_range=(z_pos(end):z_pos(1)); %如【5,6,7.。。56】，参考上
    end

    %-----------------------------计算每个轴方向的网格索引对应的网格实际大小
    % begin 读取geometry文件中的网格大小数据
    geo_file = fullfile(root_dir, simu_name, 'geometry.input'); %系统的网格划分文件
    [idx_grid_x,size_grid_x]=get_grid_size(geo_file,'X');       %在geometry input 文件里搜索对应的网格对应的网格大小，如第5网格对应大小1e-5mm
    %index_array对应网格索引数组，例如【23 24 56 124】（这个是根据.input文件确定的
    %size_array是对应的网格索引对应的其网格尺寸，如【1e-5 2e-5 1e-5 4e-5】
    [idx_grid_y,size_grid_y]=get_grid_size(geo_file,'Y');
    [idx_grid_z,size_grid_z]=get_grid_size(geo_file,'Z');       %分别求出X Y Z轴上的网格尺寸数据的索引数组。

    %begin ------------------------计算 X Y Z上每一个grid的实际大小尺寸，保存为对应数组
    grid_x=zeros(x_size,1);
    for m = 1:x_size
        grid_x(m)= calc_grid(idx_grid_x,size_grid_x,x_range(m));%每个x网格索引对应的其x方向网格大小，下同
    end
    grid_x=zeros(x_size,1);
    for m = 1:y_size
        grid_y(m)= calc_grid(idx_grid_y,size_grid_y,y_range(m));
    end
    grid_z=zeros(z_size,1);
    for m = 1:z_size
        grid_z(m)= calc_grid(idx_grid_z,size_grid_z,z_range(m));
    end
       
    %----------------- 读取 XY 面 上数据处理    
    z_slice = 0;
    E_x = reshape(E_x_unsort(1+z_slice*x_size*y_size:(z_slice+1)*y_size*x_size), y_size, x_size);
    E_y = reshape(E_y_unsort(1+z_slice*x_size*y_size:(z_slice+1)*y_size*x_size), y_size, x_size);
    E_z = reshape(E_z_unsort(1+z_slice*x_size*y_size:(z_slice+1)*y_size*x_size), y_size, x_size);
    E_rho = zeros(size(E_x));
    E_phi = zeros(size(E_x));
    for m = 1:length(x_range)
        for n = 1:length(y_range)
            vec_rho = [m n]-[x_range(round(end/2)) 300];
            vec_rho = vec_rho./norm(vec_rho);            
            vec_x = [1 0];
            vec_y = [0 1];
            vec_phi = [-vec_rho(2) vec_rho(1)];
            
            E_rho(n,m) = abs(vec_x*vec_rho'*E_x(n,m) + vec_y*vec_rho'*E_y(n,m));
            E_phi(n,m) = abs(vec_x*vec_phi'*E_x(n,m) + vec_y*vec_phi'*E_y(n,m));
        end
    end
    E_tot = abs((E_x.^2+E_y.^2+E_z.^2).^(1/2));
    
    E_rho_set{idx} = E_rho;
    E_phi_set{idx} = E_phi;
    E_z_set{idx} = E_z;
    E_x_set{idx} = E_x;
    E_y_set{idx} = E_y;
    E_tot_set{idx} = E_tot;
    x_range_set{idx} = x_range;
    y_range_set{idx} = y_range;
    z_range_set{idx} = z_range;
end

%%
d = 0.2;
clear E_z_circ
clear E_phi_circ
clear E_rho_circ
for m = 1:360
    E_z_circ(m) = E_z(round(sin(m/180*pi)*(0.5+d)/0.0025+300), round(cos(m/180*pi)*(0.5+d)/0.0025+300));
    E_phi_circ(m) = E_phi(round(sin(m/180*pi)*(0.5+d)/0.0025+300), round(cos(m/180*pi)*(0.5+d)/0.0025+300));
    E_rho_circ(m) = E_rho(round(sin(m/180*pi)*(0.5+d)/0.0025+300), round(cos(m/180*pi)*(0.5+d)/0.0025+300));
end

mean(20*log10(abs(E_rho_circ./E_phi_circ)));

figure
hold on
% plot(20*log10(abs(E_rho_circ./E_phi_circ)),'k')
plot(20*log10(abs(E_z_circ)),'k','linewidth',2)
plot(20*log10(abs(E_rho_circ)),'r','linewidth',2)
plot(20*log10(abs(E_phi_circ)),'b','linewidth',2)
axis([0 360 -300 -100])
ylabel('[dB]')
xlabel('degree')
legend('E_z','E_\rho','E_\phi')
set(gca,'FontSize',24)

figure
agle = (abs(E_z_circ)).^2./(abs(E_phi_circ).^2+abs(E_z_circ).^2);
% plot(acos(agle)/pi*180)
plot(agle)
figure
agle = (abs(E_z_circ)).^2./(abs(E_rho_circ).^2+abs(E_z_circ).^2);
plot(agle)
% plot(acos(agle)/pi*180)
axis([0 360 0 90])

figure
agle = (abs(E_rho_circ)).^2./(abs(E_rho_circ).^2+abs(E_phi_circ).^2);
plot(agle)
% plot(acos(agle)/pi*180)
axis([0 360 0 90])

%%
deg = (90+90)*pi/180;
d = 0:0.002:0.2;
clear E_phi_circ
clear E_rho_circ
clear E_z_circ
for m = 1:length(d)
    E_z_circ(m) = E_z(round(sin(deg)*(0.5+d(m))/0.0025+300), round(cos(deg)*(0.5+d(m))/0.0025+300));
    E_phi_circ(m) = E_phi(round(sin(deg)*(0.5+d(m))/0.0025+300), round(cos(deg)*(0.5+d(m))/0.0025+300));
    E_rho_circ(m) = E_rho(round(sin(deg)*(0.5+d(m))/0.0025+300), round(cos(deg)*(0.5+d(m))/0.0025+300));   
end

mean(20*log10(abs(E_rho_circ./E_phi_circ)));

figure
hold on
% plot((abs(E_rho_circ./E_phi_circ)),'k')
plot(20*log10(abs(E_z_circ)),'k')
plot(20*log10(abs(E_rho_circ)),'r')
plot(20*log10(abs(E_phi_circ)),'b')
axis([0 360 -200 50])
% figure
% agle = (abs(E_rho_circ)).^2./(abs(E_rho_circ).^2+abs(E_phi_circ).^2);
% plot(acos(agle)/pi*180)
% axis([0 100 0 90])

%%
% figure
% surf(x_range*grid_y(1)-0.75, y_range*grid_y(1)-0.75, 20*log10(abs(E_z)));
% view(0, 90)
% shading interp
% axis equal
% axis([0 1.5 0 1.5]-0.75)
% caxis([-260 -100])
% colormap hot
% colorbar
% ylabel('x[m]')
% xlabel('y[m]')
% set(gca,'FontSize',24)

% figure
% surf(x_range*grid_y(1), y_range*grid_y(1), 20*log10(abs(E_rho)));
% view(0, 90)
% shading interp
% axis equal
% axis([0 1.5 0 1.5])
% caxis([-260 -100])
% colormap hot
% colorbar
% 
% figure
% surf(x_range*grid_y(1), y_range*grid_y(1), 20*log10(abs(E_phi)));
% view(0, 90)
% shading interp
% axis equal
% axis([0 1.5 0 1.5])
% caxis([-260 -100])
% colormap hot
% colorbar
% 
% figure
% surf(x_range*grid_y(1), y_range*grid_y(1), 20*log10(E_tot));
% view(0, 90)
% shading interp
% axis equal
% axis([0 1.5 0 1.5])
% caxis([-260 -100])
% colormap hot
% colorbar
% 
% ylabel('x[m]')
% xlabel('y[m]')
% set(gca,'FontSize',24)