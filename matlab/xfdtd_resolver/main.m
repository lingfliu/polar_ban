%XFDTD result resolver for version 7.3
% Details in Reference Manual
% Appendix D (Appendix of Calculation Engine Output Files)
% D.2 Multi Point Sensor

clear all;clc;

root_dir = 'D:\xfdtd\';
simu_name = 'irho_surface_multi_planes_adv.xf\Simulations\000001\Run0001';
sensor_name = 'MultiPoint_plane_z100_1';

result_file = fullfile(root_dir, simu_name, 'output', sensor_name);

info_file = [result_file '_info.bin'];
geom_file = [result_file '_geom.bin'];
 
info = get_info(info_file);

[E_x_unsort E_y_unsort E_z_unsort] = get_unsort_E_field(result_file, 1, info.num_point); 

x_pos = zeros(1,info.num_point);%初始化一个空间
y_pos = zeros(1,info.num_point);
z_pos = zeros(1,info.num_point);
fid = fopen( geom_file );
for k = 1:info.num_point;
    x_pos(k) = fread( fid, 1, '*uint32' );%可能是 x1 x1 x1 x1 x1 x1  x2 x2 x2 x2 x2 x2  x3 x3....每个'xn'数字个数是包括一个x截面的数据量的个数
    y_pos(k) = fread( fid, 1, '*uint32' );%可能是 y1 y1 y1 y2 y2 y2  y1 y1 y1 y2 y2 y2  y1 y1....每个xn yn zn其实是这个节点对应的gird的索引，不一定从0开始
    z_pos(k) = fread( fid, 1, '*uint32' );%可能是 z1 z2 z2 z1 z2 z3  z1 z2 z3 z1 z2 z3  z1 z2....
end
fclose(fid);


% 计算每个轴上划分了多少个网格，并把“网格索引”分别建立成数组
x_size=abs(x_pos(end)-x_pos(1)+1);      %这个方向的网格的个数，根据实际情况而定 如56-5=51个网格
y_size=abs(y_pos(end)-y_pos(1)+1);
z_size=abs(z_pos(end)-z_pos(1)+1);

if x_pos(end) > x_pos(1)
    x_range=(x_pos(1):x_pos(end)); %如【5,6,7.。。56】，参考上
else 
    x_range=(x_pos(end):x_pos(1)); %如【5,6,7.。。56】，参考上
end
if y_pos(end) > y_pos(1)
    y_range=(y_pos(1):y_pos(end)); %如【5,6,7.。。56】，参考上
else 
    y_range=(y_pos(end):y_pos(1)); %如【5,6,7.。。56】，参考上
end
if z_pos(end) > z_pos(1)
    z_range=(z_pos(1):z_pos(end)); %如【5,6,7.。。56】，参考上
else 
    z_range=(z_pos(end):z_pos(1)); %如【5,6,7.。。56】，参考上
end
% y_range=(y_pos(1):y_pos(end)); %对应着y轴网格的索引值数组
% z_range=(z_pos(1):z_pos(end));

%-----------------------------计算每个轴方向的网格索引对应的网格实际大小
% begin 读取geometry文件中的网格大小数据
geo_file = fullfile(root_dir, simu_name, 'geometry.input'); %系统的网格划分文件
[idx_grid_x,size_grid_x]=get_grid_size(geo_file,'X');       %在geometry input 文件里搜索对应的网格对应的网格大小，如第5网格对应大小1e-5mm
                                                            %index_array对应网格索引数组，例如【23 24 56 124】（这个是根据.input文件确定的
                                                            %size_array是对应的网格索引对应的其网格尺寸，如【1e-5 2e-5 1e-5 4e-5】
[idx_grid_y,size_grid_y]=get_grid_size(geo_file,'Y');
[idx_grid_z,size_grid_z]=get_grid_size(geo_file,'Z');       %分别求出X Y Z轴上的网格尺寸数据的索引数组。

%begin ------------------------计算 X Y Z上每一个grid的实际大小尺寸，保存为对应数组
grid_x=zeros(x_size,1);
for m = 1:x_size
 grid_x(m)= calc_grid(idx_grid_x,size_grid_x,x_range(m));%每个x网格索引对应的其x方向网格大小，下同
end
grid_x=zeros(x_size,1);
for m = 1:y_size
 grid_y(m)= calc_grid(idx_grid_y,size_grid_y,y_range(m));
end
grid_z=zeros(z_size,1);
for m = 1:z_size
 grid_z(m)= calc_grid(idx_grid_z,size_grid_z,z_range(m));
end
 
 %------------------------------------------ 读取  YZ面 上数据处理

% begin 对某一X轴的横截面 YZ面 上的电场强度绘图
%x_slice=0;    %对solid sensor的话x_slice可取不等于0（从0开始算起）
              %对surface、planer sensor，如YZ面sensor,必须设置为0，x_slice=0.
              %下同
%value=reshape(dataExyz(1+x_slice*ysize*zsize:(x_slice+1)*abs(ysize)*zsize),zsize,abs(ysize));                                                                                
%plot(z_index_array,value)  %画出x=0横截图面的图像
%save('Exyz_YZ','value');
% end



%---------------------------------------- 读取 XZ 面 上数据处理

% begin 对某一Y轴的横截面 XZ面 上的电场强度绘图
%y_slice=0;      % 将要读取的第几个y截面（从0开始）
                %对solid sensor的话y_slice可取不等于0
                %对 一个面如xz sensor,必须y_slice=0.
%value=reshape(dataExyz(1+y_slice*xsize*zsize:(y_slice+1)*xsize*zsize),zsize,xsize);                                                                                 
%mesh(x_index_array,z_index_array,value)  %画出x=0横截图面的图像
%save('Exyz_XZ','value');

% end




%%
%----------------- 读取 XY 面 上数据处理

z_slice = 0;
E_x = reshape(E_x_unsort(1+z_slice*x_size*y_size:(z_slice+1)*y_size*x_size), y_size, x_size);
E_y = reshape(E_y_unsort(1+z_slice*x_size*y_size:(z_slice+1)*y_size*x_size), y_size, x_size);
E_z = reshape(E_z_unsort(1+z_slice*x_size*y_size:(z_slice+1)*y_size*x_size), y_size, x_size);
E_rho = zeros(size(E_x));
E_phi = zeros(size(E_x));
for m = 1:length(x_range)
    for n = 1:length(y_range)
        vec_rho = [m n]*grid_y(1)-[0.5 0.5];
        vec_rho = vec_rho./abs(vec_rho);
        vec_x = [1 0];
        vec_y = [0 1];
        vec_phi = [-vec_rho(2) vec_rho(1)];
        
        E_rho(n,m) = abs(vec_x*vec_rho'*E_x(n,m) + vec_y*vec_rho'*E_y(n,m));        
        E_phi(n,m) = abs(vec_x*vec_phi'*E_x(n,m) + vec_y*vec_phi'*E_y(n,m));        
    end
end


figure
surf(x_range*grid_y(1), y_range*grid_y(1), 10*log10(E_rho));
E_tot = abs((E_x.^2+E_y.^2+E_z.^2).^(1/2));
figure
surf(x_range*grid_y(1), y_range*grid_y(1), 10*log10(E_tot)); 

% x_slice = 0;
% E_x = reshape(E_x_unsort(1+x_slice*y_size*z_size:(x_slice+1)*y_size*z_size), z_size, y_size);
% E_y = reshape(E_y_unsort(1+x_slice*y_size*z_size:(x_slice+1)*y_size*z_size), z_size, y_size);
% E_z = reshape(E_z_unsort(1+x_slice*y_size*z_size:(x_slice+1)*y_size*z_size), z_size, y_size);
% E_rho = zeros(size(E_x));
% for m = 1:length(y_range)
%     for n = 1:length(z_range)
%         vec_rho = [m n]*grid_y(1)-[0.5 0.5];
%         vec_rho = vec_rho./abs(vec_rho);
%         vec_x = [1 0];
%         vec_y = [0 1];
%         
%         E_rho(n,m) = abs(vec_x*vec_rho'*E_x(n,m) + vec_y*vec_rho'*E_y(n,m));        
%     end
% end
% 
% figure
% surf(z_range*grid_y(1), y_range*grid_y(1), 10*log10(E_rho));
% E_tot = abs((E_x.^2+E_y.^2+E_z.^2).^(1/2));
% figure
% surf(z_range*grid_y(1), y_range*grid_y(1), 10*log10(E_tot)); 


% E_x = reshape(E_x_unsort, z_size, x_size);
% E_y = reshape(E_y_unsort, z_size, x_size);
% E_z = reshape(E_z_unsort, z_size, x_size);
% E_rho = zeros(size(E_x));
% for m = 1:length(x_range)
%     for n = 1:length(z_range)
%         vec_rho = [m n]*grid_y(1)-[0.5 0.5];
%         vec_rho = vec_rho./abs(vec_rho);
%         vec_x = [1 0];
%         vec_y = [0 1];
%         
%         E_rho(n,m) = abs(vec_x*vec_rho'*E_x(n,m) + vec_y*vec_rho'*E_y(n,m));        
%     end
% end
% 
% figure
% surf(x_range*grid_y(1), z_range*grid_y(1), 10*log10(E_rho));
% E_tot = abs((E_x.^2+E_y.^2+E_z.^2).^(1/2));
% figure
% surf(x_range*grid_y(1), z_range*grid_y(1), 10*log10(E_tot)); 